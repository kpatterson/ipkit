#ifndef _KHPtech_IPKit_IPTCPPacket_h
#define _KHPtech_IPKit_IPTCPPacket_h
/*
 *  IPTCPPacket.h
 *  KHPtech Shared Components / IPKit
 *
 *  Created by Kevin H. Patterson on 2/11/11.
 *  Copyright 2011 Kevin H. Patterson, KHPtech. All rights reserved.
 *
 */

#include <cstdint>
#include <cstddef>
#include <sys/types.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netinet/ip_var.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <arpa/inet.h>
#include <net/if.h>

#ifdef IPSEC
#include <netinet6/ipsec.h>
#endif /*IPSEC*/

#include <string>

#include "IPKit/TCPUDPEndpoint.h"


class IPTCPPacket {
public:
	// Allocate space for a packet, call Init( const uint8_t& i_Packet ) later.
	IPTCPPacket( size_t i_Size );

	// Copy i_Packet
	IPTCPPacket( const uint8_t& i_Packet, size_t i_Size );

	// Point to i_Packet, optionally taking ownership of the pointer (ala delete[])
	IPTCPPacket( uint8_t* i_Packet, size_t i_Size, bool i_TakeOwnership = false );

	// Copy i_Packet (construct first with IPTCPPacket( size_t i_Size ))
	bool Init( const uint8_t& i_Packet );
	
	~IPTCPPacket() {
		if( m_Owns ) delete[] m_Packet;
	}
	
	const uint8_t* RawPacket() const {
		return m_Packet;
	}
	
	size_t RawSize() const {
		return (m_Data - m_Packet) + m_DataSize;
	}
	
	const struct ip* IPHeader() const {
		return m_IPHeader;
	}
	
	size_t IPHeaderSize() const {
		if( !m_IPHeader ) return 0;
		return m_IPHeader->ip_hl * 4;
	}
	
	IPv4Adx SrcIPv4Adx() const {
		return IPv4Adx( ntohl( m_IPHeader->ip_src.s_addr ) );
	}
	
	uint32_t SrcIPv4AdxN() const {
		return m_IPHeader->ip_src.s_addr;
	}
	
	void SetSrcIPv4Adx( IPv4Adx i_Adx ) {
		m_IPHeader->ip_src.s_addr = htonl( i_Adx );
		m_IPHeaderGood = false;
	}
	
	void SetSrcIPv4AdxN( uint32_t i_Adx ) {
		m_IPHeader->ip_src.s_addr = i_Adx;
		m_IPHeaderGood = false;
	}
	
	IPv4Adx DstIPv4Adx() const {
		return IPv4Adx( ntohl( m_IPHeader->ip_dst.s_addr ) );
	}
	
	uint32_t DstIPv4AdxN() const {
		return m_IPHeader->ip_dst.s_addr;
	}
	
	void SetDstIPv4Adx( IPv4Adx i_Adx ) {
		m_IPHeader->ip_dst.s_addr = htonl( i_Adx );
		m_IPHeaderGood = false;
	}
	
	void SetDstIPv4AdxN( uint32_t i_Adx ) {
		m_IPHeader->ip_dst.s_addr = i_Adx;
		m_IPHeaderGood = false;
	}
/*
	std::string SrcIPv4AdxStr() const {
		return inet_ntoa( m_IPHeader->ip_src );
	}
	
	std::string DstIPv4AdxStr() const {
		return inet_ntoa( m_IPHeader->ip_dst );
	}
*/
	const struct tcphdr* TCPHeader() const {
		return m_TCPHeader;
	}
	
	size_t TCPHeaderSize() const {
		if( !m_TCPHeader ) return 0;
		return m_TCPHeader->th_off * 4;
	}
	
	TCPUDPPort SrcPort() const {
		return ntohs( m_TCPHeader->th_sport );
	}
	
	void SetSrcPort( TCPUDPPort i_Port ) {
		m_TCPHeader->th_sport = htons( i_Port );
		m_TCPHeaderGood = false;
	}
	
	TCPUDPPort DstPort() const {
		return ntohs( m_TCPHeader->th_dport );
	}
	
	void SetDstPort( TCPUDPPort i_Port ) {
		m_TCPHeader->th_dport = htons( i_Port );
		m_TCPHeaderGood = false;
	}
	
	uint16_t SrcPortN() const {
		return m_TCPHeader->th_sport;
	}
	
	uint16_t DstPortN() const {
		return m_TCPHeader->th_dport;
	}
	
	uint8_t* Data() {
		return m_Data;
	}
	
	std::string DataStr() const {
		return std::string( reinterpret_cast<char*>( m_Data ), m_DataSize );
	}
	
	size_t DataSize() const {
		return m_DataSize;
	}
	
	TCPUDPEndpoint SrcEndpoint() const {
		return TCPUDPEndpoint( SrcIPv4Adx(), SrcPort() );
	}
	
	void SetSrcEndpoint( const TCPUDPEndpoint& i_Endpoint ) {
		SetSrcIPv4Adx( i_Endpoint.IPv4Address );
		SetSrcPort( i_Endpoint.Port );
	}
	
	TCPUDPEndpoint DstEndpoint() const {
		return TCPUDPEndpoint( DstIPv4Adx(), DstPort() );
	}

	void SetDstEndpoint( const TCPUDPEndpoint& i_Endpoint ) {
		SetDstIPv4Adx( i_Endpoint.IPv4Address );
		SetDstPort( i_Endpoint.Port );
	}
	
	void FixHeaders( bool i_Force = true );
	
	void Dump();

private:
	void Init( size_t i_Size );
	
private:
	uint8_t* m_Packet;
	struct ip* m_IPHeader;
	struct tcphdr* m_TCPHeader;
	uint8_t* m_Data;
	size_t m_DataSize;
	
	bool m_IPHeaderGood;
	bool m_TCPHeaderGood;
	
	bool m_Owns;
};

#endif // _KHPtech_IPKit_IPTCPPacket_h
