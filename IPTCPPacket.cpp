/*
 *  IPTCPPacket.cpp
 *  KHPtech Shared Components / IPKit
 *
 *  Created by Kevin H. Patterson on 2/11/11.
 *  Copyright 2011 Kevin H. Patterson, KHPtech. All rights reserved.
 *
 */

#include "IPKit/IPTCPPacket.h"
#include "IPKit/InetChecksum.h"
#include "StringUtil/StdStringUtil.h"

#include <iostream>

using namespace std;


IPTCPPacket::IPTCPPacket( size_t i_Size )
: m_Packet( 0 )
, m_IPHeader( 0 )
, m_TCPHeader( 0 )
, m_Data( 0 )
, m_DataSize( 0 )
, m_IPHeaderGood( false )
, m_TCPHeaderGood( false )
, m_Owns( true )
{
	if( sizeof( struct ip ) > i_Size ) {
		cerr << "IPTCPPacket::IPTCPPacket(): WARNING: Packet too small to contain IP header. Dropping packet." << endl;
		return;
	}

	m_Packet = new uint8_t[i_Size];
	m_DataSize = i_Size;
}


// Copy i_Packet
IPTCPPacket::IPTCPPacket( const uint8_t& i_Packet, size_t i_Size )
: m_Packet( 0 )
, m_IPHeader( 0 )
, m_TCPHeader( 0 )
, m_Data( 0 )
, m_DataSize( 0 )
, m_IPHeaderGood( false )
, m_TCPHeaderGood( false )
, m_Owns( true )
{
	if( sizeof( struct ip ) > i_Size ) {
		cerr << "IPTCPPacket::IPTCPPacket(): WARNING: Packet too small to contain IP header. Dropping packet." << endl;
		return;
	}
	
	m_Packet = new uint8_t[i_Size];
	memcpy( m_Packet, &i_Packet, i_Size );

	Init( i_Size );
}


// Point to i_Packet, optionally taking ownership of the pointer (ala delete[])
IPTCPPacket::IPTCPPacket( uint8_t* i_Packet, size_t i_Size, bool i_TakeOwnership )
: m_Packet( i_Packet )
, m_IPHeader( 0 )
, m_TCPHeader( 0 )
, m_Data( 0 )
, m_DataSize( 0 )
, m_IPHeaderGood( false )
, m_TCPHeaderGood( false )
, m_Owns( i_TakeOwnership )
{
	if( sizeof( struct ip ) > i_Size ) {
		cerr << "IPTCPPacket::IPTCPPacket(): WARNING: Packet too small to contain IP header. Dropping packet." << endl;
		return;
	}
	
	Init( i_Size );
}


// Copy i_Packet (construct first with IPTCPPacket( size_t i_Size ))
bool IPTCPPacket::Init( const uint8_t& i_Packet ) {
	if( m_IPHeader ) return false;
	
	size_t i_Size = m_DataSize;
	m_DataSize = 0;
	
	memcpy( m_Packet, &i_Packet, i_Size );
	
	Init( i_Size );
	
	return true;
}


void IPTCPPacket::Init( size_t i_Size ) {
	m_IPHeader = reinterpret_cast<struct ip*>( m_Packet );
	m_IPHeaderGood = true;
	
	if( ntohs( m_IPHeader->ip_len ) > i_Size ) {
		cerr << "IPTCPPacket::IPTCPPacket(): WARNING: Packet is smaller than size specified in IP header. Dropping packet." << endl;
		cerr << "  ip_len: " << ntohs( m_IPHeader->ip_len ) << " Size: " << i_Size << endl;
		cerr << "  DATA: " << KHP::DataToStringHex( m_Packet, i_Size ) << endl;
		m_IPHeaderGood = false;
		m_IPHeader = 0;
		return;
	} else if( ntohs( m_IPHeader->ip_len ) > i_Size ) {
		cerr << "IPTCPPacket::IPTCPPacket(): WARNING: Packet is larger than size specified in IP header. Truncating packet." << endl;
		cerr << "  ip_len: " << ntohs( m_IPHeader->ip_len ) << " Size: " << i_Size << endl;
		cerr << "  DATA: " << KHP::DataToStringHex( m_Packet, i_Size ) << endl;
		i_Size = ntohs( m_IPHeader->ip_len );
	}
	
	if( m_IPHeader->ip_p == IPPROTO_TCP ) {
		if( IPHeaderSize() + sizeof( struct tcphdr ) > i_Size ) {
			cerr << "IPTCPPacket::IPTCPPacket(): WARNING: Packet too small to contain TCP header. Dropping TCP header." << endl;
			cerr << "  ip_hl: " << IPHeaderSize() << " sizeof( struct ip ): " << sizeof( struct ip ) << endl;
			cerr << "  DATA: " << KHP::DataToStringHex( m_Packet, i_Size ) << endl;
			m_Data = 0;
			return;
		}
		
		m_TCPHeader = reinterpret_cast<struct tcphdr*>( m_Packet + IPHeaderSize() );
		m_TCPHeaderGood = true;
		
		m_Data = m_Packet + IPHeaderSize() + TCPHeaderSize();
		
		if( m_Data - m_Packet > ssize_t( i_Size ) ) {
			cerr << "IPTCPPacket::IPTCPPacket(): WARNING: Packet too small to contain Extended TCP header. Dropping TCP header." << endl;
			cerr << "  th_off: " << TCPHeaderSize() << " sizeof( struct tcphdr ): " << sizeof( struct tcphdr ) << endl;
			cerr << "  DATA: " << KHP::DataToStringHex( m_Packet, i_Size ) << endl;
			m_TCPHeaderGood = false;
			m_TCPHeader = 0;
			m_Data = 0;
			return;
		}
		
		m_DataSize = i_Size - TCPHeaderSize() - IPHeaderSize();
		
		if( m_Data + m_DataSize - m_Packet > ssize_t( i_Size ) ) {
			cerr << "IPTCPPacket::IPTCPPacket(): WARNING: Packet too small to contain TCP data. Truncating TCP data." << endl;
			cerr << "  DATA: " << KHP::DataToStringHex( m_Packet, i_Size ) << endl;
			m_DataSize = i_Size - (m_Data - m_Packet);
			m_TCPHeaderGood = false;
			return;
		}
	}
}


void IPTCPPacket::FixHeaders( bool i_Force ) {
	size_t TotalSize = m_DataSize;
	
	if( m_TCPHeader ) {
		TotalSize += TCPHeaderSize();

		if( !m_TCPHeaderGood || i_Force ) {
			// calculate TCP Checksum
			struct {	// TCP pseudo header used in checksum calculation
				struct in_addr src;
				struct in_addr dst;
				uint8_t pad;
				uint8_t proto;
				uint16_t tcp_len;
			} tcppsh;
			
			tcppsh.src.s_addr = m_IPHeader->ip_src.s_addr;
			tcppsh.dst.s_addr = m_IPHeader->ip_dst.s_addr;
			tcppsh.pad = 0;
			tcppsh.proto = IPPROTO_TCP;
			tcppsh.tcp_len = htons( TotalSize );
			
			m_TCPHeader->th_sum = 0;
			m_TCPHeader->th_sum = InetChecksum( reinterpret_cast<uint8_t*>( &tcppsh ), sizeof( tcppsh ), InetDataSum( reinterpret_cast<uint8_t*>( m_TCPHeader ), m_Data - reinterpret_cast<uint8_t*>( m_TCPHeader ) + m_DataSize ) );
			
			m_TCPHeaderGood = true;
		}
	}
	
	if( m_IPHeader ) {
		TotalSize += IPHeaderSize();
		
		if( !m_IPHeaderGood || i_Force ) {
			m_IPHeader->ip_len = htons( TotalSize );
			
			// calculate IP Checksum
			m_IPHeader->ip_sum = 0;
			m_IPHeader->ip_sum = InetChecksum( reinterpret_cast<uint8_t*>( m_IPHeader ), IPHeaderSize() );
			
			m_IPHeaderGood = true;
		}
	}
}

void IPTCPPacket::Dump() {
	cout << "IPTCPPacket::Dump()" << endl;
	cout << "  m_Packet*: " << (void*)m_Packet << " Owns: " << m_Owns << endl;
	cout << "  m_IPHeader*: " << (void*)m_IPHeader << " Good: " << m_IPHeaderGood << endl;
	cout << "  m_TCPHeader*: " << (void*)m_TCPHeader << " Good: " << m_TCPHeaderGood << endl;
	cout << "  m_Data*: " << (void*)m_Data << " Size: " << m_DataSize << endl;
	cout << "  Src: " << SrcEndpoint().ToString() << " Dst: " << DstEndpoint().ToString() << endl;
	cout << "  RAW DATA: " << KHP::DataToStringHex( m_Packet, RawSize() ) << endl;
}
