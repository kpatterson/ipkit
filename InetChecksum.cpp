/*
 *  InetChecksum.cpp
 *  KHPtech Shared Components / IPKit
 *
 *  Created by Kevin H. Patterson on 2/13/11.
 *  Copyright 2011 Kevin H. Patterson, KHPtech. All rights reserved.
 *
 */

#include "IPKit/InetChecksum.h"

#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netinet/ip_var.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>

#include <cstdint>

uint16_t InetChecksum( const uint8_t* i_Segment, size_t i_Size, unsigned int DataSum ) {
	size_t WordsRemaining = i_Size;
	unsigned int Sum = DataSum;
	const uint16_t* w = reinterpret_cast<const uint16_t*>( i_Segment );
	
	while( WordsRemaining > 1 ) {
		Sum += *w++;
		WordsRemaining -= 2;
	}
	
	if( WordsRemaining > 0 )
		Sum += *reinterpret_cast<const uint8_t*>( w );
	
	while( Sum >> 16 )
		Sum = (Sum & 0xFFFF) + (Sum >> 16);
	
	return ~Sum;
}

unsigned int InetDataSum( const uint8_t* i_Data, size_t i_Size ) {
	size_t WordsRemaining = i_Size;
	unsigned int Sum = 0;
	const uint16_t* w = reinterpret_cast<const uint16_t*>( i_Data );
	
	while( WordsRemaining > 1 ) {
		Sum += *w++;
		WordsRemaining -= 2;
	}
	
	if( WordsRemaining > 0 )
		Sum += *reinterpret_cast<const uint8_t*>( w );
	
	return Sum;
}
/*
uint16_t in_cksum_udp( const uint32_t src, const uint32_t dst, const uint8_t* segment, size_t i_Size ) {
	struct psd_udp {
		struct in_addr src;
		struct in_addr dst;
		uint8_t pad;
		uint8_t proto;
		uint16_t udp_len;
	} buf;
	
	buf.src.s_addr = src;
	buf.dst.s_addr = dst;
	buf.pad = 0;
	buf.proto = IPPROTO_UDP;
	buf.udp_len = htons( i_Size );
	return InetChecksum( (uint8_t*)&buf, sizeof( struct psd_udp ), InetDataSum( segment, i_Size ) );
}
*/