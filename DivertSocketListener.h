#ifndef _DivertSocketListener_h
#define _DivertSocketListener_h
/*
 *  DivertSocketListener.h
 *  netaccess2
 *
 *  Created by Kevin H. Patterson on 2/2/11.
 *  Copyright 2011 Kevin H. Patterson, KHPtech. All rights reserved.
 *
 */

#include <stdint.h>
#include <netinet/in.h>

#include "AppKit/Threadable.h"
#include "IPKit/TCPUDPPort.h"

#include <string>

class IPTCPPacket;

class DivertSocketListener
: public Threadable
{
public:
	DivertSocketListener( TCPUDPPort i_Port );
	virtual ~DivertSocketListener();
	
	virtual int Run();
	
protected:
	virtual void OnReceiveFrom( IPTCPPacket& i_Packet, sockaddr_in& i_ReceiveSocket ) = 0;
	
	ssize_t Reinject( IPTCPPacket& i_Packet, const sockaddr_in& i_ReceiveSocket, unsigned int i_AfterRule = 0 );
	
protected:
	TCPUDPPort m_Port;
	int	m_SocketHandle;
	sockaddr_in m_ListenSocket;
	std::string m_ClassName;
};

#endif // _DivertSocketListener_h
