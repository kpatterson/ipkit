#ifndef _KHPtech_IPKit_IPv4Adx_h
#define _KHPtech_IPKit_IPv4Adx_h
/*
 *  IPv4Adx.h
 *  KHPtech Shared Components / IPKit
 *
 *  Created by Kevin H. Patterson on 2007-07-11.
 *  Copyright 2007 Kevin H. Patterson, KHPtech. All rights reserved.
 *
 */

#include <cstdint>
#include <string>
#include <sstream>

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>


class IPv4Adx {
public:
	IPv4Adx()
	: m_Address( 0 )
	{}
	
	IPv4Adx( uint8_t i_a, uint8_t i_b, uint8_t i_c, uint8_t i_d )
	: m_Address( (i_a << 24) | (i_b << 16) | (i_c << 8) | i_d )
	{}
	
	// host byte order
	IPv4Adx( uint32_t i_Address )
	: m_Address( i_Address )
	{}
	
	// network byte order
	IPv4Adx( in_addr i_Address )
	: m_Address( ntohl( i_Address.s_addr ) )
	{}

	IPv4Adx( const std::string& i_IPv4AdxStr )
	: m_Address( 0 )
	{
		Set( i_IPv4AdxStr );
	}
	
	~IPv4Adx() {
	}
	
	operator uint32_t() const {
		return m_Address;
	}

	uint32_t operator=( const uint32_t& rhs ) {
		return m_Address = rhs;
	}

	uint32_t operator&=( const uint32_t& rhs ) {
		return m_Address &= rhs;
	}
	
	int a() const {
		return (m_Address >> 24) & 0x000000FF;
	}

	int b() const {
		return (m_Address >> 16) & 0x000000FF;
	}

	int c() const {
		return (m_Address >> 8) & 0x000000FF;
	}

	int d() const {
		return m_Address & 0x000000FF;
	}
	
	void Set( uint8_t i_a, uint8_t i_b, uint8_t i_c, uint8_t i_d ) {
		m_Address = (i_a << 24) | (i_b << 16) | (i_c << 8) | i_d;
	}

	void Set_a( uint8_t i_a ) {
		m_Address = (m_Address & 0x00FFFFFF) | (i_a << 24);
	}

	void Set_b( uint8_t i_b ) {
		m_Address = (m_Address & 0xFF00FFFF) | (i_b << 16);
	}

	void Set_c( uint8_t i_c ) {
		m_Address = (m_Address & 0xFFFF00FF) | (i_c << 8);
	}

	void Set_d( uint8_t i_d ) {
		m_Address = (m_Address & 0xFFFFFF00) | i_d;
	}

	// host byte order
	void Set( uint32_t i_Address ) {
		m_Address = i_Address;
	}
	
	// network byte order
	void Set( in_addr i_Address ) {
		m_Address = ntohl( i_Address.s_addr );
	}
	
	bool Set( const std::string& i_IPv4AdxStr ) {
		struct in_addr ina;
		if( inet_pton( AF_INET, i_IPv4AdxStr.c_str(), &ina ) < 1 )
			return false;
		
		m_Address = ntohl( ina.s_addr );
		return true;
	}

	bool Mask( unsigned int i_Mask ) {
		if( i_Mask > 31 ) return false;
		uint32_t mask = 0xFFFFFFFF;
		mask <<= (32 - i_Mask);
		uint32_t t_Address = m_Address;
		m_Address &= mask;
		return m_Address != t_Address;
	}

	IPv4Adx GetMasked( unsigned int i_Mask ) const {
		IPv4Adx result( *this );
		result.Mask( i_Mask );
		return result;
	}

	std::string ToString() const {
		std::stringstream s;
		s << a() << "." << b() << "." << c() << "." << d();
		return s.str();
	}

	bool IsZero() const {
		return m_Address == 0;
	}

private:
	uint32_t m_Address;
};

#endif // _KHPtech_IPKit_IPv4Adx_h
