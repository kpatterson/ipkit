#ifndef _KHPtech_IPKit_TCPUDPPort_h
#define _KHPtech_IPKit_TCPUDPPort_h
//
//  TCPUDPPort.h
//  netaccess2
//
//  Created by Kevin H. Patterson on 6/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include <cstdint>
#include <string>
#include <sstream>


class TCPUDPPort {
public:
	TCPUDPPort()
	: m_Port( 0 )
	{}
	
	TCPUDPPort( uint16_t i_Port )
	: m_Port( i_Port )
	{}
	
	TCPUDPPort( const std::string& i_PortStr )
	: m_Port( 0 )
	{
		Set( i_PortStr );
	}
	
	operator uint16_t() const {
		return m_Port;
	}
	
	uint16_t operator=( const uint16_t& rhs ) {
		return m_Port = rhs;
	}
	
	void Set( const std::string& i_PortStr ) {
		std::stringstream s;
		s << i_PortStr;
		s >> m_Port;
	}
	
	std::string ToString() const {
		std::stringstream s;
		s << m_Port;
		return s.str();
	}
	
private:
	uint16_t m_Port;
};

#endif // _KHPtech_IPKit_TCPUDPPort_h
