#ifndef _KHPtech_IPKit_MACAdx_h
#define _KHPtech_IPKit_MACAdx_h
/*
 *  MACAdx.h
 *  KHPtech Shared Components / IPKit
 *
 *  Created by Kevin H. Patterson on 2013-07-16.
 *  Copyright 2013 Kevin H. Patterson, KHPtech. All rights reserved.
 *
 */

#include "StringUtil/StdStringUtil.h"

#include <cstdint>
#include <stdexcept>

class MACAdx {
public:
	MACAdx()
	: m_uint64( 0 )
	{}

	MACAdx( uint8_t i_a, uint8_t i_b, uint8_t i_c, uint8_t i_d, uint8_t i_e, uint8_t i_f )
	: m_uint64( 0 )
	{
		Set( i_a, i_b, i_c, i_d, i_e, i_f );
	}

	MACAdx( const std::string& i_MACAdxStr )
	: m_uint64( 0 )
	{
		Set( i_MACAdxStr );
	}

	~MACAdx() {
	}

	uint8_t& At( int i_Index ) {
		if( i_Index < 0 || i_Index > 5 )
			throw std::out_of_range( "Index to MACAdx::operator[] is out of range. Valid values are 0 to 5." );

		return m_Bytes[i_Index];
	}

	const uint8_t& At( int i_Index ) const {
		if( i_Index < 0 || i_Index > 5 )
			throw std::out_of_range( "Index to MACAdx::operator[] is out of range. Valid values are 0 to 5." );

		return m_Bytes[i_Index];
	}

	uint8_t& operator[]( int i_Index ) {
		return m_Bytes[i_Index];
	}

	const uint8_t& operator[]( int i_Index ) const {
		return m_Bytes[i_Index];
	}

	bool operator==( const MACAdx& rhs ) const {
		return m_uint64 == rhs.m_uint64;
	}

	bool operator<( const MACAdx& rhs ) const {
		return m_uint64 < rhs.m_uint64;
	}

	int a() const {
		return m_Bytes[0];
	}

	int b() const {
		return m_Bytes[1];
	}

	int c() const {
		return m_Bytes[2];
	}

	int d() const {
		return m_Bytes[3];
	}

	int e() const {
		return m_Bytes[4];
	}

	int f() const {
		return m_Bytes[5];
	}

	void Set( uint8_t i_a, uint8_t i_b, uint8_t i_c, uint8_t i_d, uint8_t i_e, uint8_t i_f ) {
		m_Bytes[0] = i_a;
		m_Bytes[1] = i_b;
		m_Bytes[2] = i_c;
		m_Bytes[3] = i_d;
		m_Bytes[4] = i_e;
		m_Bytes[5] = i_f;
	}

	void Set_a( uint8_t i_a ) {
		m_Bytes[0] = i_a;
	}

	void Set_b( uint8_t i_b ) {
		m_Bytes[1] = i_b;
	}

	void Set_c( uint8_t i_c ) {
		m_Bytes[2] = i_c;
	}

	void Set_d( uint8_t i_d ) {
		m_Bytes[3] = i_d;
	}

	void Set_e( uint8_t i_e ) {
		m_Bytes[4] = i_e;
	}

	void Set_f( uint8_t i_f ) {
		m_Bytes[5] = i_f;
	}

	bool Set( const std::string& i_MACAdxStr ) {
		KHP::StringToDataHex( m_Bytes, 6, i_MACAdxStr );
		return true;
	}

	std::string ToString() const {
		std::stringstream s;
		s << KHP::DataToStringHex( &m_Bytes[0], 1 ) << ":" << KHP::DataToStringHex( &m_Bytes[1], 1 ) << ":" << KHP::DataToStringHex( &m_Bytes[2], 1 ) << ":" << KHP::DataToStringHex( &m_Bytes[3], 1 ) << ":" << KHP::DataToStringHex( &m_Bytes[4], 1 ) << ":" << KHP::DataToStringHex( &m_Bytes[5], 1 );
		return s.str();
	}

	const uint64_t& AsUint64() const {
		return m_uint64;
	}

	bool IsLocal() const {
		return m_Bytes[0] & 0x02;
	}

	bool IsMulticast() const {
		return m_Bytes[0] & 0x01;
	}

	bool IsBroadcast() const {
		return (m_uint64 & 0xFFFFFFFFFFFF0000LL) == 0xFFFFFFFFFFFF0000LL;
	}

	uint32_t OUI() const {
		return (m_Bytes[0] << 16) | (m_Bytes[1] << 8) | m_Bytes[2];
	}

private:
	union {
		uint8_t m_Bytes[8];
		uint64_t m_uint64;
	};
};

#endif // _KHPtech_IPKit_MACAdx_h
