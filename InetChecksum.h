#ifndef _KHPtech_IPKit_InetChecksum_h
#define _KHPtech_IPKit_InetChecksum_h
/*
 *  InetChecksum.h
 *  KHPtech Shared Components / IPKit
 *
 *  Created by Kevin H. Patterson on 2/13/11.
 *  Copyright 2011 Kevin H. Patterson, KHPtech. All rights reserved.
 *
 */

#include <cstdint>
#include <cstddef>
#include <sys/types.h>

uint16_t InetChecksum( const uint8_t* i_Segment, size_t i_Size, unsigned int i_DataSum = 0 );

unsigned int InetDataSum( const uint8_t* i_Data, size_t i_Size );


#endif // _KHPtech_IPKit_InetChecksum_h
