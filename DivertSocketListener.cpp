/*
 *  DivertSocketListener.cpp
 *  netaccess2
 *
 *  Created by Kevin H. Patterson on 2/2/11.
 *  Copyright 2011 Kevin H. Patterson, KHPtech. All rights reserved.
 *
 */

#include "DivertSocketListener.h"

#include "AppKit/AppException.h"

#include "IPKit/IPTCPPacket.h"

#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <netinet/ip_var.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <arpa/inet.h>
#include <net/if.h>

#ifdef IPSEC
#include <netinet6/ipsec.h>
#endif /*IPSEC*/

#include <ctype.h>
#include <err.h>
#include <errno.h>
#include <math.h>
#include <netdb.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <termios.h>

#include <unistd.h>

#include <string.h>

#include <iostream>
#include <sstream>
#include <map>

#include "StringUtil/StringUtil.h"
#include "KSNamespace.h"

using namespace KS::AppKit;

#define MAXBUFSIZE 65535


DivertSocketListener::DivertSocketListener( TCPUDPPort i_Port )
: m_Port( i_Port )
, m_SocketHandle( 0 )
, m_ClassName( "DivertSocketListener" )
{
	// open a divert socket
	m_SocketHandle = socket( AF_INET, SOCK_RAW, IPPROTO_DIVERT );

	if( m_SocketHandle == -1 ) {
		std::stringstream s;
		s << m_ClassName << "::" << m_ClassName << "(): Failed to open a divert socket.";
		throw AppException( 1, s.str() );
	}

	m_ListenSocket.sin_family = AF_INET;
	m_ListenSocket.sin_port = htons( m_Port );
	m_ListenSocket.sin_addr.s_addr = 0;
}


DivertSocketListener::~DivertSocketListener() {
	if( m_SocketHandle ) close( m_SocketHandle );
}


int DivertSocketListener::Run() {
	Running = true;
	
	std::cout << m_ClassName << " started on port " << m_Port << "." << std::endl;
	
	int ret = bind( m_SocketHandle, reinterpret_cast< struct sockaddr* >( &m_ListenSocket ), sizeof( struct sockaddr_in ) );
	if( ret != 0 ) {
		close( m_SocketHandle );
		m_SocketHandle = 0;
		std::cerr << "  Error bind(): " << strerror( ret ) << std::endl;
		return -1;
	}
	
	// read data in
	while( Running ) {
		uint8_t RawPacket[MAXBUFSIZE];
		ssize_t RawPacketSize = 0;
		sockaddr_in ReceiveSocket;
		{
			socklen_t sockaddr_in_size = sizeof( struct sockaddr_in );
			RawPacketSize = recvfrom( m_SocketHandle, RawPacket, MAXBUFSIZE, 0, reinterpret_cast<struct sockaddr*>( &ReceiveSocket ), &sockaddr_in_size );
		}
		if( RawPacketSize >= 0 ) {
			IPTCPPacket Packet( RawPacket, RawPacketSize, false );
			OnReceiveFrom( Packet, ReceiveSocket );
		}
	}
	
	return 0;
}


ssize_t DivertSocketListener::Reinject( IPTCPPacket& i_Packet, const sockaddr_in& i_ReceiveSocket, unsigned int i_AfterRule ) {
	i_Packet.FixHeaders();

	ssize_t SentRawPacketSize;

	if( i_AfterRule ) {
		sockaddr_in ReceiveSocket = i_ReceiveSocket;
		ReceiveSocket.sin_port = i_AfterRule;
		SentRawPacketSize = sendto( m_SocketHandle, i_Packet.RawPacket(), i_Packet.RawSize(), 0, reinterpret_cast<struct sockaddr*>( &ReceiveSocket ), sizeof( struct sockaddr_in ) );
	} else {
		SentRawPacketSize = sendto( m_SocketHandle, i_Packet.RawPacket(), i_Packet.RawSize(), 0, reinterpret_cast<struct sockaddr*>( const_cast<sockaddr_in*>( &i_ReceiveSocket ) ), sizeof( struct sockaddr_in ) );
	}
	
	if( SentRawPacketSize <= 0 ) 
		std::cerr << m_ClassName << ": Warning: reinject errno = " << errno << std::endl;
	
	return SentRawPacketSize;
}
