#ifndef _KHPtech_IPKit_TCPUDPEndpoint_h
#define _KHPtech_IPKit_TCPUDPEndpoint_h
/*
 *  TCPUDPEndpoint.h
 *  KHPtech Shared Components / IPKit
 *
 *  Created by Kevin H. Patterson on 2/18/11.
 *  Copyright 2011 Kevin H. Patterson, KHPtech. All rights reserved.
 *
 */

#include "IPKit/IPv4Adx.h"
#include "IPKit/TCPUDPPort.h"

#include <cstdint>

#include <string>
#include <sstream>


class TCPUDPEndpoint {
public:
	TCPUDPEndpoint()
	: IPv4Address( 0 )
	, Port( 0 )
	{
	}
	
	TCPUDPEndpoint( IPv4Adx i_IPv4Address, TCPUDPPort i_Port )
	: IPv4Address( i_IPv4Address )
	, Port( i_Port )
	{
	}
	
	bool operator<( const TCPUDPEndpoint& rhs ) const {
		if( IPv4Address < rhs.IPv4Address ) return true;
		if( IPv4Address > rhs.IPv4Address ) return false;
		if( Port < rhs.Port ) return true;
//		if( Port > rhs.Port ) return false;
		return false;
	}
	
	std::string ToString() const {
		std::stringstream s;
		s << IPv4Address.ToString() << ":" << Port;
		return s.str();
	}
	
public:
	IPv4Adx IPv4Address;
	TCPUDPPort Port;
};

#endif // _KHPtech_IPKit_TCPUDPEndpoint
