#ifndef _KHPtech_IPKit_RawTCPConnection_h
#define _KHPtech_IPKit_RawTCPConnection_h
/*
 *  RawTCPConnection.h
 *  KHPtech Shared Components / IPKit	
 *
 *  Created by Kevin H. Patterson on 2/18/11.
 *  Copyright 2011 Kevin H. Patterson, KHPtech. All rights reserved.
 *
 */

#include "IPKit/TCPUDPEndpoint.h"
#include "SystemClock/SystemClock.h"

#include <netinet/tcp.h>
#include <cstdint>

class RawTCPConnection {
public:
	enum m_State_e {
		Unknown = 0x00,
		Open_SYN = 0x01,
		Open_SYN_ACK = 0x02,
		Open_ACK = 0x04,
		Open = 0x07,
		Open_Mask = 0xF3,
		Local_FIN = 0x10,
		Remote_ACK = 0x20,
		Remote_FIN = 0x40,
		Local_ACK = 0x80,
		Closed = 0xF0,
		Closed_Mask = 0xF0
	};

	RawTCPConnection()
	: m_Src()
	, m_Dst()
	, m_LastActivityTime( SystemClock::RawUSeconds() )
	, m_State( Unknown )
	{}
	
	RawTCPConnection( const TCPUDPEndpoint& i_Src, const TCPUDPEndpoint& i_Dst, int i_State = Unknown )
	: m_Src( i_Src )
	, m_Dst( i_Dst )
	, m_LastActivityTime( SystemClock::RawUSeconds() )
	, m_State( i_State )
	{}
	
	const TCPUDPEndpoint& SrcEndpoint() const {
		return m_Src;
	}
	
	const TCPUDPEndpoint& DstEndpoint() const {
		return m_Dst;
	}

	void SetActivity( uint64_t i_CurrentTime = 0 ) {
		if( i_CurrentTime == 0 )
			m_LastActivityTime = SystemClock::RawUSeconds();
		else
			m_LastActivityTime = i_CurrentTime;
	}
	
	double IdleTime( uint64_t i_CurrentTime = 0 ) {
		uint64_t tTime = i_CurrentTime;
		if( tTime == 0 )
			tTime = SystemClock::RawUSeconds();
		
		if( tTime < m_LastActivityTime ) {
			m_LastActivityTime = tTime;
			return 0.0;
		}
		return (tTime - m_LastActivityTime) / 1000000.0;
	}
	
	void UpdateState( int i_TCPFlags, bool i_Remote = false ) {
		switch( m_State ) {
			case Unknown:
				if( (i_TCPFlags & (TH_SYN | TH_ACK)) == TH_SYN ) m_State = Open_ACK;
				break;
			case Open_SYN:
				if( (i_TCPFlags & (TH_SYN | TH_ACK)) == (TH_SYN | TH_ACK) ) m_State = Open_SYN_ACK;
				break;
			case Open_SYN_ACK:
				if( (i_TCPFlags & (TH_SYN | TH_ACK)) == TH_ACK ) m_State = Open_ACK;
				break;
		}
		if( (m_State & Open_Mask) == Open ) {
			if( i_Remote ) {
				if( (i_TCPFlags & TH_FIN) == TH_FIN ) m_State |= Remote_FIN;
				if( (m_State & Local_FIN) && ((i_TCPFlags & TH_ACK) == TH_ACK) ) m_State |= Remote_ACK;
			} else {
				if( (i_TCPFlags & TH_FIN) == TH_FIN ) m_State |= Local_FIN;
				if( (m_State & Remote_FIN) && ((i_TCPFlags & TH_ACK) == TH_ACK) ) m_State |= Local_ACK;
			}
		}
	}
	
private:
	TCPUDPEndpoint m_Src;
	TCPUDPEndpoint m_Dst;
	
	uint64_t m_LastActivityTime;
	int m_State;
};

#endif // _KHPtech_IPKit_RawTCPConnection_h
